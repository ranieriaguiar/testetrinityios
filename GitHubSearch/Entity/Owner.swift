//
//  Owner.swift
//  GitHubSearch
//
//  Created by Ranieri Aguiar on 07/05/19.
//  Copyright © 2019 Ranieri. All rights reserved.
//

import SwiftyJSON

class Owner {
    
    init(json: JSON) {
        id = json["id"].int32Value
        login = json["login"].stringValue
        imageUrl = json["avatar_url"].stringValue
    }
    
    var id: Int32
    var login: String
    var imageUrl: String
}
