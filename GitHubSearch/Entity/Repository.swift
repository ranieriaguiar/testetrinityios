//
//  Repository.swift
//  GitHubSearch
//
//  Created by Ranieri Aguiar on 07/05/19.
//  Copyright © 2019 Ranieri. All rights reserved.
//

import SwiftyJSON

class Repository {
    
    init(json: JSON) {
        id = json["id"].int32Value
        name = json["name"].stringValue
        description = json["description"].stringValue
        star = json["stargazers_count"].intValue
        forks = json["forks_count"].intValue
        owner = Owner(json: json["owner"])
    }
    
    var id: Int32
    var name: String
    var description: String
    var star: Int
    var forks: Int
    var owner: Owner
}
