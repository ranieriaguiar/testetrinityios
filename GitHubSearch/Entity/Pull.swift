//
//  Pull.swift
//  GitHubSearch
//
//  Created by Ranieri Aguiar on 07/05/19.
//  Copyright © 2019 Ranieri. All rights reserved.
//

import SwiftyJSON

class Pull {
    
    init(json: JSON) {
        id = json["id"].int32Value
        title = json["title"].stringValue
        body = json["body"].stringValue
        url = json["html_url"].stringValue
        user = Owner(json: json["user"])
        
        let jsonDate = json["created_at"].stringValue
        creationDate = jsonDate.dateConverter(jsonDate)
    }
    
    var id: Int32
    var title: String
    var body: String
    var url: String
    var creationDate: String
    var user: Owner
}
