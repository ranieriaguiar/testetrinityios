//
//  PullTableViewCell.swift
//  GitHubSearch
//
//  Created by Ranieri Aguiar on 08/05/19.
//  Copyright © 2019 Ranieri. All rights reserved.
//

import UIKit

class PullTableViewCell: UITableViewCell {
    
    @IBOutlet var lbName: UILabel!
    @IBOutlet var lbTitle: UILabel!
    @IBOutlet var lbBody: UILabel!
    @IBOutlet var lbDate: UILabel!
    @IBOutlet var imgAvatar: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setupCell(_ pull: Pull) {
        lbName.text = pull.user.login
        lbTitle.text = pull.title
        lbBody.text = pull.body
        lbDate.text = pull.creationDate
        loadImage(url: pull.user.imageUrl)
    }
    
    private func loadImage(url: String) {
        if let image = Requester.shared.cachedImage(for: url) {
            populate(with: image)
            return
        }
        downloadImage(for: url)
    }
    
    private func downloadImage(for url: String) {
        Requester.shared.retrieveImage(for: url) { image in
            self.populate(with: image)
        }
    }
    
    private func populate(with image: UIImage) {
        imgAvatar.image = image
    }
}
