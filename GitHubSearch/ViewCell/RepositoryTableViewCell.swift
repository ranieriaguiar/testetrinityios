//
//  RepositoryTableViewCell.swift
//  GitHubSearch
//
//  Created by Ranieri Aguiar on 07/05/19.
//  Copyright © 2019 Ranieri. All rights reserved.
//

import UIKit

class RepositoryTableViewCell: UITableViewCell {

    @IBOutlet var lbName: UILabel!
    @IBOutlet var lbDesc: UILabel!
    @IBOutlet var lbStar: UILabel!
    @IBOutlet var lbFork: UILabel!
    @IBOutlet var lbOwner: UILabel!
    @IBOutlet var imgAvatar: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setupCell(_ repository: Repository) {
        lbName.text = repository.name
        lbDesc.text = repository.description
        lbStar.text = String(repository.star)
        lbFork.text = String(repository.forks)
        lbOwner.text = repository.owner.login
        loadImage(url: repository.owner.imageUrl)
    }
    
    private func loadImage(url: String) {
        if let image = Requester.shared.cachedImage(for: url) {
            populate(with: image)
            return
        }
        downloadImage(for: url)
    }
    
    private func downloadImage(for url: String) {
        Requester.shared.retrieveImage(for: url) { image in
            self.populate(with: image)
        }
    }
    
    private func populate(with image: UIImage) {
        imgAvatar.image = image
    }
}
