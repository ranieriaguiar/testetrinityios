//
//  String.swift
//  GitHubSearch
//
//  Created by Ranieri Aguiar on 11/05/19.
//  Copyright © 2019 Ranieri. All rights reserved.
//

import Foundation

extension String {
    
    func dateConverter(_ stringDate: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        if let date = dateFormatter.date(from: stringDate) {
            dateFormatter.dateFormat = "dd-MM-yyyy"
            return dateFormatter.string(from: date)
        } else {
            return ""
        }
    }
}
