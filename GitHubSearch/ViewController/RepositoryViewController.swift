//
//  RepositoryViewController.swift
//  GitHubSearch
//
//  Created by Ranieri Aguiar on 07/05/19.
//  Copyright © 2019 Ranieri. All rights reserved.
//

import UIKit

class RepositoryViewController: UIViewController {
    
    @IBOutlet var tableView: UITableView!
    
    let viewControllerID = "pullVC"
    let repositoryCellIdentifier = "RepositoryTableViewCell"
    let refreshControl = UIRefreshControl()
    var repositories = [Repository]()
    var lastPage = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initViews()
        requestRepository(page: lastPage)
    }
    
    func initViews() {
        navigationItem.backBarButtonItem = UIBarButtonItem()
        
        let nibSearch = UINib.init(nibName: repositoryCellIdentifier, bundle: nil)
        tableView.register(nibSearch, forCellReuseIdentifier: repositoryCellIdentifier)
        tableView.dataSource = self
        tableView.delegate = self
        
        refreshControl.attributedTitle = NSAttributedString(string: "Puxe para buscar mais...")
        refreshControl.addTarget(self, action: #selector(refresh(sender:)), for: UIControl.Event.valueChanged)
        tableView.addSubview(refreshControl)
    }
    
    @objc func refresh(sender:AnyObject) {
        requestRepository(page: lastPage)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == viewControllerID {
            let view = segue.destination as! PullViewController
            let repository = sender as! Repository
            view.repository = repository
        }
    }
    
    func updateTable(data: [Repository]) {
        var rows = [IndexPath]()
        let startIndex = repositories.count
        let lastIndex = startIndex + (data.count - 1)
        for index in startIndex ... lastIndex {
            let indexPath = IndexPath(row: index, section: 0)
            rows.append(indexPath)
        }
        repositories.append(contentsOf: data)
        tableView.beginUpdates()
        tableView.insertRows(at: rows, with: .left)
        tableView.endUpdates()
    }
    
    func requestRepository(page: Int) {
        func onStart() {
            
        }
        func onError(msg: String) {
            presentAlertView(msg: msg)
            refreshControl.endRefreshing()
        }
        func onSuccess(repositoriesResponse: [Repository]) {
            refreshControl.endRefreshing()
            tableView.isHidden = false
            updateTable(data: repositoriesResponse)
            lastPage += 1
            print("repositories - count: \(repositories.count)")
        }
        if !Requester.isRequesting {
            Requester.shared.getRepository(page: page, onStart: onStart, onError: onError, onSuccess: onSuccess)
        } else {
            refreshControl.endRefreshing()
        }
    }
}

extension RepositoryViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return repositories.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 265
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let repository = repositories[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: repositoryCellIdentifier, for: indexPath) as! RepositoryTableViewCell
        cell.setupCell(repository)
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row > repositories.count - 10 {
            requestRepository(page: lastPage)
            print("prefetched - page: \(lastPage)")
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let repository = repositories[indexPath.row]
        performSegue(withIdentifier: viewControllerID, sender: repository)
    }
}
