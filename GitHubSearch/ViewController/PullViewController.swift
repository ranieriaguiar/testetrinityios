//
//  PullViewController.swift
//  GitHubSearch
//
//  Created by Ranieri Aguiar on 08/05/19.
//  Copyright © 2019 Ranieri. All rights reserved.
//

import UIKit

class PullViewController: UIViewController {
    
    @IBOutlet var tableView: UITableView!
    
    let viewControllerID = "webVC"
    let pullCellIdentifier = "PullTableViewCell"
    let refreshControl = UIRefreshControl()
    var repository: Repository!
    var pulls = [Pull]()
    var lastPage = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initViews()
        requestPull(page: lastPage)
    }
    
    func initViews() {
        title = repository.name
        
        let nibSearch = UINib.init(nibName: pullCellIdentifier, bundle: nil)
        tableView.register(nibSearch, forCellReuseIdentifier: pullCellIdentifier)
        tableView.dataSource = self
        tableView.delegate = self
        
        refreshControl.attributedTitle = NSAttributedString(string: "Puxe para buscar mais...")
        refreshControl.addTarget(self, action: #selector(refresh(sender:)), for: UIControl.Event.valueChanged)
        tableView.addSubview(refreshControl)
    }
    
    @objc func refresh(sender:AnyObject) {
        requestPull(page: lastPage)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == viewControllerID {
            let view = segue.destination as! WebViewController
            let url = sender as! URL
            view.url = url
        }
    }
    
    func updateTable(data: [Pull]) {
        var rows = [IndexPath]()
        let startIndex = pulls.count
        let lastIndex = startIndex + (data.count - 1)
        for index in startIndex ... lastIndex {
            let indexPath = IndexPath(row: index, section: 0)
            rows.append(indexPath)
        }
        pulls.append(contentsOf: data)
        tableView.beginUpdates()
        tableView.insertRows(at: rows, with: .left)
        tableView.endUpdates()
    }
    
    func requestPull(page: Int) {
        func onStart() {
            
        }
        func onError(msg: String) {
            presentAlertView(msg: msg)
            refreshControl.endRefreshing()
        }
        func onSuccess(pullsResponse: [Pull]) {
            refreshControl.endRefreshing()
            tableView.isHidden = false
            updateTable(data: pullsResponse)
            lastPage += 1
            print("pulls - count: \(pulls.count)")
        }
        if !Requester.isRequesting {
            Requester.shared.getPull(repository: repository, page: page, onStart: onStart, onError: onError, onSuccess: onSuccess)
        } else {
            refreshControl.endRefreshing()
        }
    }
}

extension PullViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pulls.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 230
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let pull = pulls[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: pullCellIdentifier, for: indexPath) as! PullTableViewCell
        cell.setupCell(pull)
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row > pulls.count - 10 {
            requestPull(page: lastPage)
            print("prefetched - page: \(lastPage)")
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let mediaURL = pulls[indexPath.row].url
        if let url = URL(string: mediaURL) {
            performSegue(withIdentifier: viewControllerID, sender: url)
        } else {
            presentAlertView(msg: "URL inválida!")
        }
    }
}
