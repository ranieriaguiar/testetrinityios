//
//  WebViewController.swift
//  GitHubSearch
//
//  Created by Ranieri Aguiar on 08/05/19.
//  Copyright © 2019 Ranieri. All rights reserved.
//

import UIKit
import WebKit

class WebViewController: UIViewController {
    
    var webView: WKWebView!
    var url: URL!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        webView = WKWebView()
        view = webView
        loadWebPage()
    }
    
    private func loadWebPage() {
        let request = URLRequest(url: url)
        webView.load(request)
    }
}
