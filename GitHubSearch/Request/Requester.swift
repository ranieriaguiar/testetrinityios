//
//  Requester.swift
//  GitHubSearch
//
//  Created by Ranieri Aguiar on 07/05/19.
//  Copyright © 2019 Ranieri. All rights reserved.
//

import Alamofire
import AlamofireImage
import SwiftyJSON

class Requester {
    static let shared = Requester()
    static var isRequesting = false
    static let qtdPerPage = 25
    
    private let imageCache = AutoPurgingImageCache()
    private let urlGitHub = "https://api.github.com/"
    
    func getRepository(qtdPerPage: Int = Requester.qtdPerPage,
                       page: Int,
                       onStart: () -> Void,
                       onError: @escaping (String) -> Void,
                       onSuccess: @escaping ([Repository]) -> Void) {
        let url = "\(urlGitHub)search/repositories?q=language:Java&sort=stars&page=\(page)&per_page=\(qtdPerPage)"
        onStart()
        Requester.isRequesting = true
        Alamofire.request(url, method: .get)
            .validate()
            .responseJSON { response in
                
                switch response.result {
                case .success:
                    let json = JSON(response.result.value!)
                    var repositories = [Repository]()
                    
                    for item in json["items"].arrayValue {
                        repositories.append(Repository(json: item))
                    }
                    onSuccess(repositories)
                    
                case .failure(let error):
                    print(error.localizedDescription)
                    onError(error.localizedDescription)
                }
                Requester.isRequesting = false
        }
    }
    
    func getPull(repository: Repository,
                 qtdPerPage: Int = Requester.qtdPerPage,
                 page: Int,
                 onStart: () -> Void,
                 onError: @escaping (String) -> Void,
                 onSuccess: @escaping ([Pull]) -> Void) {
        let url = "\(urlGitHub)repos/\(repository.owner.login)/\(repository.name)/pulls?per_page=\(qtdPerPage)"
        onStart()
        Requester.isRequesting = true
        Alamofire.request(url, method: .get)
            .validate()
            .responseJSON { response in
                
                switch response.result {
                case .success:
                    let json = JSON(response.result.value!)
                    var pulls = [Pull]()
                    
                    for item in json.arrayValue {
                        pulls.append(Pull(json: item))
                    }
                    onSuccess(pulls)
                    
                case .failure(let error):
                    print(error.localizedDescription)
                    onError(error.localizedDescription)
                }
                Requester.isRequesting = false
        }
    }
    
    func retrieveImage(for url: String, onSuccess: @escaping (UIImage) -> Void) {
        Alamofire.request(url, method: .get).responseImage { response in
            
            guard let image = response.result.value else { return }
            onSuccess(image)
            self.cache(image, for: url)
        }
    }
    
    //MARK: = Image Caching
    
    func cache(_ image: Image, for url: String) {
        imageCache.add(image, withIdentifier: url)
    }
    
    func cachedImage(for url: String) -> Image? {
        return imageCache.image(withIdentifier: url)
    }
}
