//
//  GitHubSearchTests.swift
//  GitHubSearchTests
//
//  Created by Ranieri Aguiar on 10/05/19.
//  Copyright © 2019 Ranieri. All rights reserved.
//

import XCTest

@testable import GitHubSearch

var lastPage: Int!
var repositories: [Repository]!

class GitHubSearchTests: XCTestCase {
    
    override func setUp() {
        lastPage = 1
        repositories = [Repository]()
    }
    
    override func tearDown() {
        lastPage = 1
        repositories = nil
    }
    
    func testRequestRepositoryPagination() {
        let promise = expectation(description: "Status code: 200")
        requestRepository(promise: promise)
        waitForExpectations(timeout: 25, handler: nil)
    }
    
    func requestRepository(promise: XCTestExpectation) {
        func onStart() {
            
        }
        func onError(msg: String) {
            XCTFail("Error: \(msg.description)")
        }
        func onSuccess(repositoriesResponse: [Repository]) {
            repositories.append(contentsOf: repositoriesResponse)
            XCTAssertEqual(repositories.count, Requester.qtdPerPage * lastPage, "A busca está correta.")
            if lastPage <= 1 {
                lastPage += 1
                requestRepository(promise: promise)
                promise.fulfill()
            }
        }
        Requester.shared.getRepository(page: lastPage, onStart: onStart, onError: onError, onSuccess: onSuccess)
    }
}
