# Criar um aplicativo de consulta a API do [GitHub](https://github.com)#

Criar um aplicativo para consultar a [API do GitHub](https://developer.github.com/v3/) e trazer os reposit�rios mais populares de Java. Basear-se no mockup fornecido:

![bitbucket.png](https://bitbucket.org/repo/bApLBb/images/1070562783-bitbucket.png)
### **Deve conter** ###

- __Lista de reposit�rios__. Exemplo de chamada na API: `https://api.github.com/search/repositories?q=language:Java&sort=stars&page=1`
  * Pagina��o na tela de lista, com endless scroll / scroll infinito (incrementando o par�metro `page`).
  * Cada reposit�rio deve exibir Nome do reposit�rio, Descri��o do Reposit�rio, Nome / Foto do autor, N�mero de Stars, N�mero de Forks
  * Ao tocar em um item, deve levar a lista de Pull Requests do reposit�rio
- __Pull Requests de um reposit�rio__. Exemplo de chamada na API: `https://api.github.com/repos/<criador>/<reposit�rio>/pulls`
  * Cada item da lista deve exibir Nome / Foto do autor do PR, T�tulo do PR, Data do PR e Body do PR
  * Ao tocar em um item, deve abrir no browser a p�gina do Pull Request em quest�o

### **A solu��o DEVE conter** ##

* Vers�o m�nima do iOS : 8.*
* Arquivo .gitignore
* Usar Storyboard e Autolayout
* Gest�o de depend�ncias no projeto. Ex: Cocoapods
* Framework para Comunica��o com API. Ex:  AFNetwork
* Mapeamento json -> Objeto . Ex: [Mantle](https://github.com/Mantle/Mantle#mtlmodel)

### **Ganha + pontos se conter** ###

* Testes unit�rios no projeto. Ex: XCTests / Specta + Expecta
* Testes funcionais. Ex: KIF
* App Universal , Ipad | Iphone | Landscape | Portrait (Size Classes)
* Cache de Imagens. Ex SDWebImage

### **Sugest�es** ###

As sugest�es de bibliotecas fornecidas s�o s� um guideline, sintam-se a vontade para usar diferentes e nos surpreenderem. O importante de fato � que os objetivos macros sejam atingidos. =)

### **OBS** ###

A foto do mockup � meramente ilustrativa.  


### **Processo de submiss�o** ###

O candidato dever� implementar a solu��o e enviar um pull request para este reposit�rio com a solu��o.

O processo de Pull Request funciona da seguinte maneira:

1. Candidato far� um fork desse reposit�rio (n�o ir� clonar direto!)
2. Far� seu projeto nesse fork.
3. Commitar� e subir� as altera��es para o __SEU__ fork.
4. Pela interface do Bitbucket, ir� enviar um Pull Request.

Se poss�vel deixar o fork p�blico para facilitar a inspe��o do c�digo.

### **ATEN��O** ###

N�o se deve tentar fazer o PUSH diretamente para ESTE reposit�rio!