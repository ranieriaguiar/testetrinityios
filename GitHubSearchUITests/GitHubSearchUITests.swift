//
//  GitHubSearchUITests.swift
//  GitHubSearchUITests
//
//  Created by Ranieri Aguiar on 11/05/19.
//  Copyright © 2019 Ranieri. All rights reserved.
//

import XCTest

var app: XCUIApplication!

class GitHubSearchUITests: XCTestCase {

    override func setUp() {
        app = XCUIApplication()
        app.launch()
        continueAfterFailure = false
    }

    override func tearDown() {
    }

    // FIXME: Teste incorreto!
    func testSwipeUpPagination() {
        let lastPage = 5
        for _ in 1...lastPage {
            app.swipeUp()
            sleep(1)
        }
        XCTAssertEqual(app.tables.cells.count, lastPage * 25, "Deveria ter \(lastPage * 25) células!")
    }

}
